﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FractionDemo;

namespace ConsoleFraction
{
    internal class Program
    {

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            // Створюємо дроби
            // Запитуємо користувача про значення дробів
            Console.Write("Введіть чисельник для першої дробу: ");
            int numerator1 = int.Parse(Console.ReadLine());
           

            Console.Write("Введіть знаменник для першої дробу: ");
            int denominator1 = int.Parse(Console.ReadLine());
            Console.Clear();

            Console.Write("Введіть чисельник для другої дробу: ");
            int numerator2 = int.Parse(Console.ReadLine());

            Console.Write("Введіть знаменник для другої дробу: ");
            int denominator2 = int.Parse(Console.ReadLine());
            Console.Clear();

            Console.Write("Введіть чисельник для третьої дробу: ");
            int numerator3 = int.Parse(Console.ReadLine());

            Console.Write("Введіть знаменник для третьої дробу: ");
            int denominator3 = int.Parse(Console.ReadLine());
            Console.Clear();

            // Створюємо дроби зі значеннями, введеними користувачем
            Fraction f1 = new Fraction(numerator1, denominator1);
            Fraction f2 = new Fraction(numerator2, denominator2);
            Fraction f3 = new Fraction(numerator3, denominator3);

            // Демонструємо роботу арифметичних операцій
            Fraction result1 = f1 + f2;
            Fraction result2 = f1 - f2;
            Fraction result3 = f1 * f2;
            Fraction result4 = f1 / f2;

            // Демонструємо роботу операцій порівняння
            bool isEqual1 = f1 == f2;
            bool isEqual2 = f1 != f2;
            bool isGreater1 = f1 > f2;
            bool isGreaterOrEqual1 = f1 >= f2;
            bool isLess1 = f1 < f2;
            bool isLessOrEqual1 = f1 <= f2;

            // Демонструємо роботу методу для скорочення дробу
            f3.Reduce();

            // Виводимо результати
            Console.WriteLine("Виводимо ваші дроби:" + f1 + " " + f2 + " " + f3);
            Console.WriteLine("Результати арифметичних операцій:");
            Console.WriteLine($"{f1} + {f2} = {result1}");
            Console.WriteLine($"{f1} - {f2} = {result2}");
            Console.WriteLine($"{f1} * {f2} = {result3}");
            Console.WriteLine($"{f1} / {f2} = {result4}");

            Console.WriteLine();

            Console.WriteLine("Результати операцій порівняння:");
            Console.WriteLine($"{f1} == {f2} = {isEqual1}");
            Console.WriteLine($"{f1} != {f2} = {isEqual2}");
            Console.WriteLine($"{f1} > {f2} = {isGreater1}");
            Console.WriteLine($"{f1} >= {f2} = {isGreaterOrEqual1}");
            Console.WriteLine($"{f1} < {f2} = {isLess1}");
            Console.WriteLine($"{f1} <= {f2} = {isLessOrEqual1}");

            Console.WriteLine();


            Console.WriteLine($"Скорочений дріб {f3}");


            Console.ReadLine();
        }
    }
}

