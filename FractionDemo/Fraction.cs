﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FractionDemo
{
    public class Fraction
    {
        private int numerator;
        private int denominator;

        public Fraction(int numerator, int denominator)
        {
            if (denominator == 0)
            {
                throw new ArgumentException("Denominator cannot be zero.");
            }
            this.numerator = numerator;
            this.denominator = denominator;
        }

        public double Reduce()
        {
            int gcd = GCD(numerator, denominator);
            numerator /= gcd;
            denominator /= gcd;
            return gcd;
        }

        public override string ToString()
        {
            return $"{numerator}/{denominator}";
        }

        public static Fraction operator +(Fraction f1, Fraction f2)
        {
            int num = f1.numerator * f2.denominator + f2.numerator * f1.denominator;
            int den = f1.denominator * f2.denominator;
            return new Fraction(num, den);
        }

        public static Fraction operator -(Fraction f1, Fraction f2)
        {
            int num = f1.numerator * f2.denominator - f2.numerator * f1.denominator;
            int den = f1.denominator * f2.denominator;
            return new Fraction(num, den);
        }

        public static Fraction operator *(Fraction f1, Fraction f2)
        {
            int num = f1.numerator * f2.numerator;
            int den = f1.denominator * f2.denominator;
            return new Fraction(num, den);
        }

        public static Fraction operator /(Fraction f1, Fraction f2)
        {
            if (f2.numerator == 0)
            {
                throw new DivideByZeroException("Cannot divide by zero.");
            }
            int num = f1.numerator * f2.denominator;
            int den = f1.denominator * f2.numerator;
            return new Fraction(num, den);
        }

        public static bool operator ==(Fraction f1, Fraction f2)
        {
            if (ReferenceEquals(f1, f2))
            {
                return true;
            }
            if (f1 is null || f2 is null)
            {
                return false;
            }
            f1.Reduce();
            f2.Reduce();
            return f1.numerator == f2.numerator && f1.denominator == f2.denominator;
        }

        public static bool operator !=(Fraction f1, Fraction f2)
        {
            return !(f1 == f2);
        }

        public static bool operator >(Fraction f1, Fraction f2)
        {
            f1.Reduce();
            f2.Reduce();
            return (f1.numerator * f2.denominator) > (f2.numerator * f1.denominator);
        }

        public static bool operator <(Fraction f1, Fraction f2)
        {
            f1.Reduce();
            f2.Reduce();
            return (f1.numerator * f2.denominator) < (f2.numerator * f1.denominator);
        }

        public static bool operator >=(Fraction f1, Fraction f2)
        {
            return (f1 == f2) || (f1 > f2);
        }

        public static bool operator <=(Fraction f1, Fraction f2)
        {
            return (f1 == f2) || (f1 < f2);
        }

        public static Fraction operator +(Fraction f)
        {
            return f;
        }

        public static Fraction operator -(Fraction f)
        {
            return new Fraction(-f.numerator, f.denominator);
        }

        public static implicit operator double(Fraction f)
        {
            return (double)f.numerator / f.denominator;
        }

        private static int GCD(int a, int b)
        {
            a = Math.Abs(a);
            b = Math.Abs(b);
            if (b == 0)
            {
                return a;
            }
            return GCD(b, a % b);
        }
    }
}
